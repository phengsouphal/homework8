package com.example.gamer.retrofithomework.entity;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;


import com.google.gson.annotations.SerializedName;


public class CateResponse implements Parcelable {

	@SerializedName("msg")
	private String msg;

	@SerializedName("code")
	private String code;

	@SerializedName("data")
	private List<Category> data;

	@SerializedName("status")
	private boolean status;

	public void setMsg(String msg){
		this.msg = msg;
	}

	public String getMsg(){
		return msg;
	}

	public void setCode(String code){
		this.code = code;
	}

	public String getCode(){
		return code;
	}

	public void setData(List<Category> data){
		this.data = data;
	}

	public List<Category> getData(){
		return data;
	}

	public void setStatus(boolean status){
		this.status = status;
	}

	public boolean isStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"CateResponse{" +
			"msg = '" + msg + '\'' + 
			",code = '" + code + '\'' + 
			",data = '" + data + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}


	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(this.msg);
		dest.writeString(this.code);
		dest.writeTypedList(this.data);
		dest.writeByte(this.status ? (byte) 1 : (byte) 0);
	}

	public CateResponse() {
	}

	protected CateResponse(Parcel in) {
		this.msg = in.readString();
		this.code = in.readString();
		this.data = in.createTypedArrayList(Category.CREATOR);
		this.status = in.readByte() != 0;
	}

	public static final Creator<CateResponse> CREATOR = new Creator<CateResponse>() {
		@Override
		public CateResponse createFromParcel(Parcel source) {
			return new CateResponse(source);
		}

		@Override
		public CateResponse[] newArray(int size) {
			return new CateResponse[size];
		}
	};
}