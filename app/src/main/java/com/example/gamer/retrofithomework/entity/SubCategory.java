package com.example.gamer.retrofithomework.entity;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class SubCategory implements Parcelable {

	@SerializedName("des")
	private String des;

	@SerializedName("cate_name")
	private String cateName;

	@SerializedName("total_url")
	private int totalUrl;

	@SerializedName("id")
	private int id;

	@SerializedName("icon_name")
	private String iconName;

	@SerializedName("status")
	private boolean status;

	public void setDes(String des){
		this.des = des;
	}

	public String getDes(){
		return des;
	}

	public void setCateName(String cateName){
		this.cateName = cateName;
	}

	public String getCateName(){
		return cateName;
	}

	public void setTotalUrl(int totalUrl){
		this.totalUrl = totalUrl;
	}

	public int getTotalUrl(){
		return totalUrl;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setIconName(String iconName){
		this.iconName = iconName;
	}

	public String getIconName(){
		return iconName;
	}

	public void setStatus(boolean status){
		this.status = status;
	}

	public boolean isStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"SubCategory{" +
			"des = '" + des + '\'' + 
			",cate_name = '" + cateName + '\'' + 
			",total_url = '" + totalUrl + '\'' + 
			",id = '" + id + '\'' + 
			",icon_name = '" + iconName + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}


	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(this.des);
		dest.writeString(this.cateName);
		dest.writeInt(this.totalUrl);
		dest.writeInt(this.id);
		dest.writeString(this.iconName);
		dest.writeByte(this.status ? (byte) 1 : (byte) 0);
	}

	public SubCategory() {
	}

	protected SubCategory(Parcel in) {
		this.des = in.readString();
		this.cateName = in.readString();
		this.totalUrl = in.readInt();
		this.id = in.readInt();
		this.iconName = in.readString();
		this.status = in.readByte() != 0;
	}

	public static final Creator<SubCategory> CREATOR = new Creator<SubCategory>() {
		@Override
		public SubCategory createFromParcel(Parcel source) {
			return new SubCategory(source);
		}

		@Override
		public SubCategory[] newArray(int size) {
			return new SubCategory[size];
		}
	};
}