package com.example.gamer.retrofithomework;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.Toast;

import com.example.gamer.retrofithomework.Callback.MyClickListener;
import com.example.gamer.retrofithomework.adapter.CategoryAdapter;
import com.example.gamer.retrofithomework.entity.CateResponse;
import com.example.gamer.retrofithomework.entity.Category;
import com.example.gamer.retrofithomework.entity.PostCategory;
import com.example.gamer.retrofithomework.entity.UpdateCateRespone;
import com.example.gamer.retrofithomework.form.UpdateCategoryForm;
import com.example.gamer.retrofithomework.service.ServiceGenerator;
import com.example.gamer.retrofithomework.api.CategoryService;


import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements MyClickListener{

    RecyclerView recyclerView;
    public static final int PICK_IMAGE=100;
    Uri imageUri;
    ImageView imageView;

    CategoryAdapter adapter;


    CategoryService categoryService;
    private CompositeDisposable compositeDisposable;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        compositeDisposable=new CompositeDisposable();
       recyclerView=findViewById(R.id.CateRecyclerview);
        

        setRecyclerView();

        categoryService=ServiceGenerator.createService(CategoryService.class);
        Observable<CateResponse> call=categoryService.findAllMainCategory();

//       call.enqueue(new Callback<CateResponse>() {
//           @Override
//           public void onResponse(Call<CateResponse> call, Response<CateResponse> response) {
//               CateResponse cateResponse=response.body();
//               adapter.addMoreCate(cateResponse.getData());
//               Log.e("ooooo",cateResponse.toString());
//           }
//
//           @Override
//           public void onFailure(Call<CateResponse> call, Throwable t) {
//                t.printStackTrace();
//           }
//       });
        compositeDisposable.add(
                 call
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(new DisposableObserver<CateResponse>() {
                    @Override
                    public void onNext(CateResponse cateResponse) {

                        adapter.addMoreCate(cateResponse.getData());

                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onComplete() {
                        Log.e("ooooo","Complete!!!");
                    }
                })

        );


    }

    public void getData()
    {
        categoryService=ServiceGenerator.createService(CategoryService.class);
        Observable<CateResponse> call=categoryService.findAllMainCategory();

//        call.enqueue(new Callback<CateResponse>() {
//            @Override
//            public void onResponse(Call<CateResponse> call, Response<CateResponse> response) {
//                CateResponse cateResponse=response.body();
//                adapter.setData(cateResponse.getData());
//                Log.e("ooooo",cateResponse.toString());
//            }
//
//            @Override
//            public void onFailure(Call<CateResponse> call, Throwable t) {
//                t.printStackTrace();
//            }
//        });
        compositeDisposable.add(
          call
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(new DisposableObserver<CateResponse>() {
                        @Override
                        public void onNext(CateResponse cateResponse) {

                            adapter.setData(cateResponse.getData());

                        }

                        @Override
                        public void onError(Throwable e) {
                            e.printStackTrace();
                        }

                        @Override
                        public void onComplete() {
                            Log.e("ooooo","Complete");
                        }
                    })
        );
    }

    private void setRecyclerView() {

        adapter=new CategoryAdapter(this);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
    }

    private void openGallery(){
        Intent gallery=new Intent(Intent.ACTION_PICK,MediaStore.Images.Media.INTERNAL_CONTENT_URI);
        startActivityForResult(gallery,PICK_IMAGE);
    }

    @Override
    public void onClicked(final int position, View v) {

        final Category category=adapter.getCategory(position);

        PopupMenu popup = new PopupMenu(MainActivity.this, v);
        popup.getMenuInflater().inflate(R.menu.pop_up_menu, popup.getMenu());


        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener(){
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.btndelete:
                     //   Toast.makeText(MainActivity.this, String.valueOf(category.getId()), Toast.LENGTH_SHORT).show();
                        Observable<CateResponse> responseCall=categoryService.deleteCategoryById(category.getId());
//                        responseCall.enqueue(new Callback<CateResponse>() {
//                            @Override
//                            public void onResponse(Call<CateResponse> call, Response<CateResponse> response) {
//                                getData();
//                                Toast.makeText(MainActivity.this, String.valueOf(category.getId()), Toast.LENGTH_SHORT).show();
//                            }
//
//                            @Override
//                            public void onFailure(Call<CateResponse> call, Throwable t) {
//
//                            }
//                        });
                        compositeDisposable.add(
                          responseCall
                                  .subscribeOn(Schedulers.io())
                                  .observeOn(AndroidSchedulers.mainThread())
                                .subscribeWith(new DisposableObserver<CateResponse>() {
                                    @Override
                                    public void onNext(CateResponse cateResponse) {
                                        getData();
                                        Toast.makeText(MainActivity.this, String.valueOf(category.getId()), Toast.LENGTH_SHORT).show();

                                    }

                                    @Override
                                    public void onError(Throwable e) {
                                            e.printStackTrace();
                                    }

                                    @Override
                                    public void onComplete() {
                                            Log.e("oooo","Complete!!!");
                                    }
                                })

                        );


                        break;
                    case R.id.btnupdate:

                        DialogFragment fragment=UpdateCateFragment.newInstance(category);
                        fragment.show(getSupportFragmentManager(),"UpdateCateFragment");
                        Toast.makeText(MainActivity.this, "Update", Toast.LENGTH_SHORT).show();
                        break;

                }
                return true;
            }
        });
        popup.show();

        }



    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);

    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
       public void onCateUpdateEvent(CateUpdateEvent event){


        final Category category=event.getCategory();
        UpdateCategoryForm form=new UpdateCategoryForm(
                category.getCateName(),
                category.getDes(),
                null,
                category.getId(),
                category.getIconName(),
                category.isStatus()
        );
        Observable<UpdateCateRespone> updateCategory=categoryService.updateCate(category.getId(),form);
//        updateCategory.enqueue(new Callback<UpdateCateRespone>() {
//            @Override
//            public void onResponse(Call<UpdateCateRespone> call, Response<UpdateCateRespone> response) {
//              adapter.updateItemOf(response.body().getCategory());
//        }
//
//            @Override
//            public void onFailure(Call<UpdateCateRespone> call, Throwable t) {
//
//            }
//        });
        compositeDisposable.add(
          updateCategory
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(new DisposableObserver<UpdateCateRespone>() {
                            @Override
                            public void onNext(UpdateCateRespone updateCateRespone) {
                                getData();
                                adapter.updateItemOf(updateCateRespone.getCategory());

                            }

                            @Override
                            public void onError(Throwable e) {
                                e.printStackTrace();
                            }

                            @Override
                            public void onComplete() {
                                Log.e("oooo","Complete");
                            }
                        })
        );


        Toast.makeText(this,event.getCategory().getCateName(),Toast.LENGTH_SHORT).show();

        }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater=getMenuInflater();
        inflater.inflate(R.menu.addmenu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        final EditText edCate,edDes;


       switch (item.getItemId()){
           case R.id.addCate:
              // Toast.makeText(this,"Add Cate",Toast.LENGTH_SHORT).show();

               AlertDialog.Builder mBuilder=new AlertDialog.Builder(MainActivity.this);
               View mView=getLayoutInflater().inflate(R.layout.fragment_add_cate,null);
               edCate=mView.findViewById(R.id.edcatename);
               edDes=mView.findViewById(R.id.edcatedes);
               imageView=mView.findViewById(R.id.imgcateupload);
               imageView.setOnClickListener(new View.OnClickListener() {
                   @Override
                   public void onClick(View v) {
                            openGallery();
                   }
               });

               mBuilder.setPositiveButton("Save", new DialogInterface.OnClickListener() {
                   @Override
                   public void onClick(DialogInterface dialog, int which) {
                       PostCategory postCategory = new PostCategory();
                       postCategory.setCate_name(edCate.getText().toString());
                       postCategory.setDes(edDes.getText().toString());
                       postCategory.setIcon_name(imageView.toString());

//                       Call<PostCategory> postCategoryCall = categoryService.addMainCategory(postCategory);
//                       postCategoryCall.enqueue(new Callback<PostCategory>() {
//                           @Override
//                           public void onResponse(Call<PostCategory> call, Response<PostCategory> response) {
//                               Toast.makeText(MainActivity.this,"Add Cate Successfully",Toast.LENGTH_SHORT).show();
//                               Log.d("onResponse: ",response.message());
//                               getData();
//
//
//                           }
//
//                           @Override
//                           public void onFailure(Call<PostCategory> call, Throwable t) {
//
//                           }
//                       });
                       Observable<PostCategory> postCategoryObservable=categoryService.addMainCategory(postCategory);
                      compositeDisposable.add(
                       postCategoryObservable
                               .subscribeOn(Schedulers.io())
                               .observeOn(AndroidSchedulers.mainThread())
                               .subscribeWith(new DisposableObserver<PostCategory>() {

                                   @Override
                                   public void onNext(PostCategory postCategory) {
                                       Toast.makeText(MainActivity.this,"Add Cate Successfully",Toast.LENGTH_SHORT).show();

                                       getData();


                                   }

                                   @Override
                                   public void onError(Throwable e) {
                                        e.printStackTrace();
                                   }

                                   @Override
                                   public void onComplete() {
                                       Log.d("onResponse: ","Complete!!!");

                                   }
                               })
                       );
                   }
               });
               mBuilder.setView(mView);
               AlertDialog dialog=mBuilder.create();
               dialog.show();



               return true;
           default:
               return super.onOptionsItemSelected(item);
       }

    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode==RESULT_OK&&requestCode==PICK_IMAGE){
            imageUri=data.getData();
            imageView.setImageURI(imageUri);

        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        compositeDisposable.clear();
    }
}
