package com.example.gamer.retrofithomework.entity;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;


import com.google.gson.annotations.SerializedName;


public class Category implements Parcelable {

	@SerializedName("des")
	private String des;

	@SerializedName("cate_name")
	private String cateName;

	@SerializedName("sub_cate")
	private List<SubCategory> subCate;

	@SerializedName("id")
	private int id;

	@SerializedName("icon_name")
	private String iconName;

	@SerializedName("status")
	private boolean status;

	public void setDes(String des){
		this.des = des;
	}

	public String getDes(){
		return des;
	}

	public void setCateName(String cateName){
		this.cateName = cateName;
	}

	public String getCateName(){
		return cateName;
	}

	public void setSubCate(List<SubCategory> subCate){
		this.subCate = subCate;
	}

	public List<SubCategory> getSubCate(){
		return subCate;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setIconName(String iconName){
		this.iconName = iconName;
	}

	public String getIconName(){
		return iconName;
	}

	public void setStatus(boolean status){
		this.status = status;
	}

	public boolean isStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"Category{" +
			"des = '" + des + '\'' + 
			",cate_name = '" + cateName + '\'' + 
			",sub_cate = '" + subCate + '\'' + 
			",id = '" + id + '\'' + 
			",icon_name = '" + iconName + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}


	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(this.des);
		dest.writeString(this.cateName);
		dest.writeList(this.subCate);
		dest.writeInt(this.id);
		dest.writeString(this.iconName);
		dest.writeByte(this.status ? (byte) 1 : (byte) 0);
	}

	public Category() {
	}

	protected Category(Parcel in) {
		this.des = in.readString();
		this.cateName = in.readString();
		this.subCate = new ArrayList<SubCategory>();
		in.readList(this.subCate, SubCategory.class.getClassLoader());
		this.id = in.readInt();
		this.iconName = in.readString();
		this.status = in.readByte() != 0;
	}

	public static final Creator<Category> CREATOR = new Creator<Category>() {
		@Override
		public Category createFromParcel(Parcel source) {
			return new Category(source);
		}

		@Override
		public Category[] newArray(int size) {
			return new Category[size];
		}
	};
}