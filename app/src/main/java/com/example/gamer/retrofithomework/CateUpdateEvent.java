package com.example.gamer.retrofithomework;

import com.example.gamer.retrofithomework.entity.Category;

public class CateUpdateEvent {


    private Category category;

    public CateUpdateEvent(Category category) {
        this.category= category;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }
}
