package com.example.gamer.retrofithomework;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.gamer.retrofithomework.entity.Category;


/**
 * A simple {@link Fragment} subclass.
 */
public class Detail_Fragment extends Fragment {

    private Category data;
    static Detail_Fragment detailFragment;
    private TextView title,phone,web,email,location;
    private ImageView imageView,inImageView;
    public static Detail_Fragment newInstance(){
        if (detailFragment==null)
            detailFragment=new Detail_Fragment();

        return detailFragment;

    }

    public void setData(Category universityData)
    {
        data=universityData;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_detail_, container, false);
        title=view.findViewById(R.id.tv_un_name);
        phone=view.findViewById(R.id.tv_un_phone);
        web=view.findViewById(R.id.tv_un_web);
        email=view.findViewById(R.id.tv_un_email);
        location=view.findViewById(R.id.tv_un_address);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if(data!=null)
        {
            title.setText(data.getCateName());
            phone.setText(data.getCateName());
//            web.setText(data.getUnWeb());
//            email.setText(data.getUnEmail());
//            location.setText(data.getUnAddress());
//            imageView.setImageResource(data.getPictsure());

        }
    }

}
