package com.example.gamer.retrofithomework.service;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class ServiceGenerator {


    private static OkHttpClient.Builder httpClient=new OkHttpClient.Builder();
    static RxJava2CallAdapterFactory rxJava2CallAdapterFactory=RxJava2CallAdapterFactory.create();
    private static Retrofit.Builder builder=new Retrofit.Builder()
            .baseUrl("http://110.74.194.125:15000/")
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(rxJava2CallAdapterFactory);

    public static <P> P createService(Class<P> serviceClass){

        Retrofit retrofit=builder.client(httpClient.build())
                .build();
        return retrofit.create(serviceClass);
    }
}
