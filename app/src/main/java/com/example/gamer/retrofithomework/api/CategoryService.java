package com.example.gamer.retrofithomework.api;

import com.example.gamer.retrofithomework.entity.CateResponse;
import com.example.gamer.retrofithomework.entity.PostCategory;
import com.example.gamer.retrofithomework.entity.UpdateCateRespone;
import com.example.gamer.retrofithomework.form.UpdateCategoryForm;

import io.reactivex.Observable;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface CategoryService {

    @POST("/api/v1/categories/create-main")
    Observable<PostCategory> addMainCategory(@Body PostCategory postCategory);


    @GET("api/v1/categories")
    Observable<CateResponse> findAllMainCategory();

    @PUT("api/v1/categories/{id}")
    Observable<UpdateCateRespone> updateCate(
            @Path("id") int id,
            @Body UpdateCategoryForm updateCategoryForm);

    @DELETE("/api/v1/categories/{id}/delete")
    Observable<CateResponse> deleteCategoryById(@Path("id") int id);
}
